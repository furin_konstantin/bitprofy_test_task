<?
IncludeModuleLangFile(__FILE__);
class CBitProfyGroupMySQL
{
	const TABLE_NAME = "b_bitprofy_group";
	function GetList() {
		global $DB;
		$strSql =
			"SELECT * FROM ".self::TABLE_NAME;
		$query = $DB->Query($strSql, false);
		$res = array();
		while($result = $query->GetNext()) {
			$res[] = $result;
		}
		return $res;
	}

	function GetListGroups() {
		$res = array();
		$groups = self::GetList();
		foreach($groups as $group) {
			$res[] = array(
				"id"=>$group["id"],
				"value"=>$group["title"]
			);
		}
		return $res;
	}
	
		
	function GetById($id) {
		if (!empty($id)) {
			global $DB;
			$strSql =
				"SELECT * FROM ".self::TABLE_NAME." WHERE `id`=".$id;
			$query = $DB->Query($strSql, false);
			$res = $query->Fetch();
		}
		return $res;
	}
}
?>