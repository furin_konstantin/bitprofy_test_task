<?
IncludeModuleLangFile(__FILE__);
class CBitProfyPersonMySQL
{
	const TABLE_NAME = "b_bitprofy_person";
	public $genders = array(
		"f",
		"m"
	);
	
	function GetById($id) {
		if (!empty($id)) {
			global $DB;
			$strSql =
				"SELECT * FROM ".self::TABLE_NAME." WHERE `id`=".$id;
			$query = $DB->Query($strSql, false);
			$res = $query->Fetch();
		}
		return $res;
	}
	
	function GetFullDataForPersonById($id) {
		$res = self::GetById($id);
		$res["GROUP"] = CBitProfyGroupMySQL::GetById($res["group_id"]);
		$res["GENDER"] = self::GetListGender($res["gender"]);
		return $res;
	}
	
	function Add($arFields)
	{
		global $DB;
		$arInsert = $DB->PrepareInsert(self::TABLE_NAME, $arFields);
		$strSql =
				"INSERT INTO ".self::TABLE_NAME."({$arInsert[0]}) ".
				"VALUES({$arInsert[1]})";
		$res = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);       
		$ID = IntVal($DB->LastID());
		return $ID;
	}     
	
	function Update($id, $arFields)
	{
		global $DB;
		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
    $strSql = "UPDATE ".self::TABLE_NAME." SET ".$strUpdate." WHERE id=".$id;
		$res = $DB->Query($strSql, false);      
		return $res;
	}
	
	function Delete($id) {
		global $DB;
		$strSql = "DELETE FROM ".self::TABLE_NAME." WHERE id=".$id;
		$res = $DB->Query($strSql, false);
		return $res;
	}

	function GetList() {
		global $DB;
		$strSql =
			"SELECT * FROM ".self::TABLE_NAME;
		$query = $DB->Query($strSql, false);
		$res = array();
		while($result = $query->GetNext()) {
			$res[] = $result;
		}
		return $res;
	}
	
	function GetFullDataForPerson() {
		global $DB;
		$data = self::GetList();
		$res = array();
		foreach($data as $i=>$arItem) {
			$arItem["GROUP"] = CBitProfyGroupMySQL::GetById($arItem["group_id"]);
			$arItem["GENDER"] = self::GetListGender($arItem["gender"]);
			$res[$i] = $arItem;
		}
		return $res;
	}
	
	function GetListGender($id = "") {
		$res = array();
		$person = new CBitProfyPersonMySQL;
		$genders = $person->genders;
		if (empty($id)) {
			foreach($genders as $gender) {
				$res[] = array(
					"id"=>$gender,
					"value"=>GetMessage("BITPROFY_".strtoupper($gender))
				);
			}
		} else {
			$res = array(
				"id"=>$id,
				"value"=>GetMessage("BITPROFY_".strtoupper($id))
			);
		}
		return $res;
	}
}
?>