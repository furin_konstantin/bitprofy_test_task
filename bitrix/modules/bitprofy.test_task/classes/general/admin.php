<?
	IncludeModuleLangFile(__FILE__);
	class CBitProfyAdmin {
		function AdminContextMenuShow(&$items) {
			$items[] = array(			
				"TEXT" => GetMessage("BITPROFY_ADMIN_ORDER_TAB_BTN_STATUS_UPDATE"),
				"ICON" => "",
				"TITLE" => GetMessage("BITPROFY_ADMIN_ORDER_TAB_BTN_STATUS_UPDATE_TITLE"),
				"LINK" => "/bitrix/admin/dpdext_ajax.php?dpdext_ajax=status_update"			
			);
		}
	}