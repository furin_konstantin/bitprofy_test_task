<? IncludeModuleLangFile(__FILE__); ?>
<? if ($action == "add"):?>
	<tr>
		<td><?=GetMessage("BITPROFY_FIELD_GROUP");?></td>
		<td>
			<select name="group_id">
				<? foreach($groups as $group):?>
					<? if ($person["group_id"] == $group["id"]) {
						$selected = "selected";
					} else {
						$selected = "";
					}?>
					<option <?=$selected?> value="<?=$group["id"]?>"><?=$group["value"]?></option>
				<? endforeach;?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?=GetMessage("BITPROFY_FIELD_FIRST_NAME");?></td>
		<td><input type="text" value="<?=$person["first_name"]?>" name="first_name"></td>
	</tr>
	<tr>
		<td><?=GetMessage("BITPROFY_FIELD_LAST_NAME");?></td>
		<td><input type="text" value="<?=$person["last_name"]?>" name="last_name"></td>
	</tr>
	<tr>
		<td><?=GetMessage("BITPROFY_FIELD_GENDER");?></td>
		<td>
			<? foreach($genders as $gender):?>
				<? if ($gender["id"] == $person["gender"]) {
					$checked = "checked";
				} else {
					$checked = "";
				}?>
				<label><input <?=$checked?> type="radio" value="<?=$gender["id"]?>" name="gender"><?=$gender["value"]?></label><br/>
			<? endforeach;?>
		</td>
	</tr>
	<tr>
		<td>
			<? if (empty($_REQUEST['person_id'])):?>
				<input type="submit" name="ADD_PERSON" value="<?=GetMessage("BITPROFY_ADD_PERSON")?>">
			<? else:?>
				<input type="hidden" value="<?=$person["id"]?>" name="person_id">
				<input type="submit" name="UPDATE_PERSON" value="<?=GetMessage("BITPROFY_UPDATE_PERSON_TEXT")?>">
			<? endif;?>
		</td>
	</tr>
<? endif;?>
<? if ($action == "view" || $action == "delete"):?>
	<tr>
		<td><?=GetMessage("BITPROFY_FIELD_GROUP");?></td>
		<td><?=GetMessage("BITPROFY_FIELD_FIRST_NAME");?></td>
		<td><?=GetMessage("BITPROFY_FIELD_LAST_NAME");?></td>
		<td><?=GetMessage("BITPROFY_FIELD_GENDER");?></td>
	</tr>
	<? foreach($persons as $arPerson):?>
		<tr>
			<td>
				<?=$arPerson["GROUP"]["title"]?>
			</td>
			<td>
				<?=$arPerson["first_name"]?>
			</td>
			<td>
				<?=$arPerson["last_name"]?>
			</td>
			<td>
				<?=$arPerson["GENDER"]["value"]?>
			</td>
			<td>
				<a href="<?=$APPLICATION->GetCurPageParam("person_id=".$arPerson["id"]."&action=update", array("person_id", "d"));?>"><?=GetMessage("BITPROFY_UPDATE_PERSON")?></a>
			</td>
			<td>
				<a href="<?=$APPLICATION->GetCurPageParam("person_id=".$arPerson["id"]."&action=delete", array("person_id", "d"));?>"><?=GetMessage("BITPROFY_DELETE_PERSON")?></a>
			</td>
		</tr>
	<? endforeach;?>
<? endif;?>