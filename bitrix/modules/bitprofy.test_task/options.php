<?
	$mid = $_REQUEST["mid"];
	if(!$USER->IsAdmin() || !CModule::IncludeModule('iblock') || !CModule::IncludeModule($mid))
		return;	
	IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
	IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$mid."/include.php");
	IncludeModuleLangFile(__FILE__);
	$aTabs = array(
		array(
			"DIV" => "edit1",
			"TAB" => GetMessage("BITPROFY_TAB_1"),
			"ICON" => "support_settings",
			"TITLE" => GetMessage("BITPROFY_TAB_1_TITLE"),
			"OPTIONS" => array(),	
		),
	);
	
	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	$tabControl->Begin();
	$bitProfyPersonMySQL = new CBitProfyPersonMySQL;
	$bitProfyGroupsMySQL = new CBitProfyGroupMySQL;
	$arGroups = $bitProfyGroupsMySQL->GetListGroups();	
	if (!empty($_POST["ADD_PERSON"])) {
		$resultAdd = $bitProfyPersonMySQL::Add($_POST);
		if ($resultAdd) {
			echo CAdminMessage::ShowMessage(array(
				"MESSAGE"=>GetMessage("BITPROFY_SUCCESS_ADD", array("id"=>$resultAdd)),
				"TYPE"=>"OK"
			));
		} else {
			echo CAdminMessage::ShowMessage(array(
				"MESSAGE"=>GetMessage("BITPROFY_ERROR_ADD", array("id"=>$resultAdd)),
				"TYPE"=>"ERROR"
			));
		}
	}
	if (!empty($_POST["UPDATE_PERSON"])) {
		$resultUpdate = $bitProfyPersonMySQL::Update($_POST["person_id"], $_POST);
		if ($resultUpdate) {
			echo CAdminMessage::ShowMessage(array(
				"MESSAGE"=>GetMessage("BITPROFY_SUCCESS_UPDATE", array("id"=>$_POST["person_id"])),
				"TYPE"=>"OK"
			));
		} else {
			echo CAdminMessage::ShowMessage(array(
				"MESSAGE"=>GetMessage("BITPROFY_ERROR_UPDATE", array("id"=>$_POST["person_id"])),
				"TYPE"=>"ERROR"
			));
		}
	}
	
	if ($_REQUEST["action"] == "delete") {
		$resultDelete = $bitProfyPersonMySQL::Delete($_REQUEST["person_id"]);
		if ($resultDelete) {
			echo CAdminMessage::ShowMessage(array(
				"MESSAGE"=>GetMessage("BITPROFY_SUCCESS_DELETE", array("id"=>$_REQUEST["person_id"])),
				"TYPE"=>"OK"
			));
		} else {
			echo CAdminMessage::ShowMessage(array(
				"MESSAGE"=>GetMessage("BITPROFY_ERROR_DELETE", array("id"=>$_REQUEST["person_id"])),
				"TYPE"=>"ERROR"
			));
		}
	}
	
	if (!empty($_REQUEST["person_id"])) {
		$person = $bitProfyPersonMySQL::GetFullDataForPersonById($_REQUEST["person_id"]);
	}
	$arPersonList = $bitProfyPersonMySQL::GetFullDataForPerson();
	if (empty($arPersonList) && $_REQUEST["action"] == "delete") {
		LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&amp;lang=".LANGUAGE_ID);
	}
?>
<form method="post" action="<? echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<? echo LANGUAGE_ID?>" id="dataload">
	<?=bitrix_sessid_post();?>
	<? $tabControl->BeginNextTab();?>
		<? if (!empty($_POST["NEW_ADD_PERSON"]) || $_REQUEST["action"] == "update"):?>
			<? $APPLICATION->IncludeFile("/bitrix/modules/".$mid."/admin/view/person_list.php", array(
				"action"=>"add",
				"genders"=>$bitProfyPersonMySQL->GetListGender(),
				"groups"=>$arGroups,
				"person"=>$person
			));?>
		<? endif;?>
		<? if (empty($_POST["NEW_ADD_PERSON"]) && !empty($arPersonList) && $_REQUEST["action"] != "update" || $_REQUEST["action"] == "delete"):?>
			<? $APPLICATION->IncludeFile("/bitrix/modules/".$mid."/admin/view/person_list.php", array(
				"persons"=>$arPersonList,
				"action"=>"view"
			));?>
		<? else:?>
			<? if (empty($_REQUEST['person_id']) && empty($_POST["NEW_ADD_PERSON"])):?>
				<tr>
					<td><?=GetMessage("BITPROFY_PERSON_EMPTY_TABLE");?></td>
				</tr>
			<? endif;?>
		<? endif;?>
	<? $tabControl->Buttons();?>
		<? if (empty($_POST["NEW_ADD_PERSON"]) && empty($_REQUEST["person_id"]) || $_REQUEST["action"] == "delete"):?>
			<input type="submit" title="<?=GetMessage("BITPROFY_PERSON_ADD_PERSON");?>" name="NEW_ADD_PERSON" value="<?=GetMessage("BITPROFY_PERSON_ADD_PERSON");?>">
		<? endif;?>
	<? $tabControl->End();?>
</form>
