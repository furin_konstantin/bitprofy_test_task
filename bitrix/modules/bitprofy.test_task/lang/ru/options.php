<?
	$MESS["BITPROFY_TAB_1"] = "Тестовое задание для БитПрофи";
	$MESS["BITPROFY_TAB_1_TITLE"] = "Тестовое задание для БитПрофи";
	
	$MESS["BITPROFY_PERSON_EMPTY_TABLE"] = "На данный момент записей не найдено!";
	$MESS["BITPROFY_PERSON_ADD_PERSON"] = "Добавить новую персону";
	
	$MESS["BITPROFY_SUCCESS_UPDATE"] = "Запись с идентификатором id успешно обновлена";
	$MESS["BITPROFY_ERROR_UPDATE"] = "Запись с идентификатором id обновить не удалось";
	$MESS["BITPROFY_SUCCESS_DELETE"] = "Запись с идентификатором id успешно удалена";
	$MESS["BITPROFY_ERROR_DELETE"] = "Запись с идентификатором id удалить не удалось";
	$MESS["BITPROFY_SUCCESS_ADD"] = "Запись с идентификатором id успешно добавлена";
	$MESS["BITPROFY_ERROR_ADD"] = "Запись с идентификатором id добавить не удалось";