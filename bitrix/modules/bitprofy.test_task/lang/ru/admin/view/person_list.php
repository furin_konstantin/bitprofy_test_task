<?
	$MESS["BITPROFY_FIELD_FIRST_NAME"] = "Имя";
	$MESS["BITPROFY_FIELD_LAST_NAME"] = "Фамилия";
	$MESS["BITPROFY_FIELD_GENDER"] = "Пол";
	$MESS["BITPROFY_FIELD_GROUP"] = "Группа";
	
	$MESS["BITPROFY_UPDATE_PERSON"] = "Редактировать";
	$MESS["BITPROFY_DELETE_PERSON"] = "Удалить";
	$MESS["BITPROFY_UPDATE_PERSON_TEXT"] = "Редактировать персону";