<?
	CModule::AddAutoloadClasses(
		"bitprofy.test_task",
		array(
			"CBitProfyAdmin" => "classes/general/admin.php",
			"CBitProfyEvents" => "classes/general/events.php",
			"CBitProfyPersonMySQL" => "classes/mysql/bitprofy_person.php",
			"CBitProfyGroupMySQL" => "classes/mysql/bitprofy_group.php"
		)
	);
?>