create table b_bitprofy_person (
   id int not null auto_increment,
   group_id int null,
	 first_name varchar(255) null,
	 last_name varchar(255) null,
	 gender enum('f','m') null,                          
   primary key (ID)
);

create table b_bitprofy_group (
   id int not null auto_increment,
   title varchar(255) null,                       
   primary key (ID)
);

insert into b_bitprofy_group (title) VALUES ('Группа 1');
insert into b_bitprofy_group (title) VALUES ('Группа 2');
insert into b_bitprofy_group (title) VALUES ('Группа 3');

