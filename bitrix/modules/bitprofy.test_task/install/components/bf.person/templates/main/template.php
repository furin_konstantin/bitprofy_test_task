<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<? if (!empty($arResult["PERSON"])):?>
	<h3><?=GetMessage("BF_LIST_PERSON");?></h3>
	<table>
		<tr>
			<td><?=GetMessage("BITPROFY_FIELD_GROUP");?></td>
			<td><?=GetMessage("BITPROFY_FIELD_FIRST_NAME");?></td>
			<td><?=GetMessage("BITPROFY_FIELD_LAST_NAME");?></td>
			<td><?=GetMessage("BITPROFY_FIELD_GENDER");?></td>
		</tr>
		<? foreach($arResult["PERSON"] as $arPerson):?>
			<tr>
				<td><?=$arPerson["GROUP"]["title"]?></td>
				<td><?=$arPerson["first_name"]?></td>
				<td><?=$arPerson["last_name"]?></td>
				<td><?=$arPerson["GENDER"]["value"]?></td>
			</tr>
		<? endforeach;?>
	</table>
	<h3><?=GetMessage("BF_LIST_PERSON_2");?></h3>
	<table>
		<tr>
			<td><?=GetMessage("BITPROFY_FIELD_GROUP");?></td>
			<td><?=GetMessage("BITPROFY_FIELD_FIRST_NAME");?></td>
			<td><?=GetMessage("BITPROFY_FIELD_LAST_NAME");?></td>
			<td><?=GetMessage("BITPROFY_FIELD_GENDER");?></td>
		</tr>
		<? foreach($arResult["PERSON_2"] as $gender=>$arItems):?>
			<tr>
				<td>
					<h3><?=GetMessage("BITPROFT_GENDER_".strtoupper($gender))?></h3>
				</td>
			</tr>
			<? foreach($arItems as $arItem):?>
				<tr>
					<td><?=$arItem["id"]?></td>
					<td><?=$arItem["GROUP"]["title"]?></td>
					<td><?=$arItem["first_name"]?></td>
					<td><?=$arItem["last_name"]?></td>
					<td><?=$arItem["GENDER"]["value"]?></td>
				</tr>
			<? endforeach;?>
		<? endforeach;?>
	</table>
	<? //Тут уже так вывел костыльно ?>
	<h3>Всего мужчин (<?=count($arResult["PERSON_3"]["m"])?>)</h3>
	<h3>Всего женщин (<?=count($arResult["PERSON_3"]["f"])?>)</h3>
	<h3>Список групп</h3>
	<table>
		<tr>
			<td>Группа</td>
			<td>Количество персон</td>
		</tr>
		<? foreach($arResult["PERSON_4"] as $groupId=>$arPersonGroups):?>
			<tr>
				<td><?=$groupId?></td>
				<td><?=count($arPersonGroups)?></td>
			</tr>
		<? endforeach;?>
	</table>
<? endif;?>