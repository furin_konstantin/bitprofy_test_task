<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<?
	$resPersons = array();
	$resPersonForGroups = array();
	foreach($arResult["PERSON"] as $arPerson) {
		if (!empty($arPerson["gender"])) {
			$resPersons[$arPerson["gender"]][] = $arPerson;
		}
		if (!empty($arPerson["group_id"])) {
			$resPersonForGroups[$arPerson["group_id"]][] = $arPerson;
		}
	}
	
	$newResPersonForGroups = array();
	foreach($resPersonForGroups as $personGroupId=>$resPersonForGroup) {
		$groupName = "";
		foreach($arResult["GROUPS"] as $group) {
			if ($personGroupId == $group["id"]) {
				$groupName = $group["value"];
				break;
			}
		}
		$newResPersonForGroups[$groupName] = $resPersonForGroup;
	}
	
	$arResult["PERSON_4"] = $newResPersonForGroups;
	$arResult["PERSON_3"] = $resPersons;
	$arResult["PERSON_2"] = array();
	foreach($resPersons as $gender=>$arItems) {
		uasort($arItems, "sortForId");
		$count = 0;
		$newItems = array();
		foreach($arItems as $arItem) {
			if ($count == 3) {
				break;
			}
			$count++;
			$newItems[] = $arItem;
		}
		$arResult["PERSON_2"][$gender] = $newItems;
	}