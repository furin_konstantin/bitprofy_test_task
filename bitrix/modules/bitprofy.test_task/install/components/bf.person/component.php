<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
CModule::IncludeModule("bitprofy.test_task");
IncludeModuleLangFile(__FILE__);
$bitProfyPersonMySQL = new CBitProfyPersonMySQL;
$arResult["GROUPS"] = CBitProfyGroupMySQL::GetListGroups();
$arResult["PERSON"] = $bitProfyPersonMySQL::GetFullDataForPerson();
$this->IncludeComponentTemplate();