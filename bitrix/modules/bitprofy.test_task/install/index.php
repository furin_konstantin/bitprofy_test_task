<?php
IncludeModuleLangFile(__FILE__);
class bitprofy_test_task extends CModule {
	public $MODULE_ID = "bitprofy.test_task";
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	function bitprofy_test_task() {
		$this->MODULE_VERSION = $arModuleInfo["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleInfo["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("BITPROFY_MODULE_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("BITPROFY_MODULE_INSTALL_DESCRIPTION");
		$this->PARTNER_NAME = GetMessage("BITPROFY_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("BITPROFY_PARTNER_URI");
	}
	
	function InstallFiles() {
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles($p.'/'.$item, $_SERVER['DOCUMENT_ROOT'].'/bitrix/components/'.$item, $ReWrite = True, $Recursive = True);
				}
				closedir($dir);
			}
		}
		return true;
	}
	
	function UnInstallFiles() {
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || !is_dir($p0 = $p.'/'.$item))
						continue;

					$dir0 = opendir($p0);
					while (false !== $item0 = readdir($dir0))
					{
						if ($item0 == '..' || $item0 == '.')
							continue;
						DeleteDirFilesEx('/bitrix/components/'.$item.'/'.$item0);
					}
					closedir($dir0);
				}
				closedir($dir);
			}
		}
		return true;
	}
	
	function InstallDB() {
		global $DB, $DBType, $APPLICATION;
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/db/".$DBType."/install.sql");
		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}
		return true;
	}
	
	function UnInstallDB() {
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/db/".$DBType."/uninstall.sql");
		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}
		return true;
	}
	
	function InstallEvents() {
		RegisterModuleDependences('main', 'OnAdminContextMenuShow', $this->MODULE_ID, 'CBitProfyEvents', 'MyOnAdminContextMenuShow');
		return true;
	}

	function UnInstallEvents() {
		UnRegisterModuleDependences('main', 'OnAdminContextMenuShow', $this->MODULE_ID, 'CBitProfyEvents', 'MyOnAdminContextMenuShow');
		return true;
	}
	
	function InstallModule() {
		$bSuccess = true;
		if ($bSuccess) {
			if (!IsModuleInstalled($this->MODULE_ID)) {
				RegisterModule($this->MODULE_ID);
			}
		}
		return $bSuccess;
	}
	
	function UnInstallModule() {
		$bSuccess = true;
		if ($bSuccess) {
			if (IsModuleInstalled($this->MODULE_ID)) {
				UnRegisterModule($this->MODULE_ID);
			}
		}
		return $bSuccess;
	}

	function DoInstall() {
		if (!$this->InstallFiles() || !$this->InstallDB() || !$this->InstallEvents()) {
			return;
		}
		$this->InstallModule();
	}

	function DoUninstall() {
		if (!$this->UnInstallFiles() || !$this->UnInstallDB() || !$this->UnInstallEvents()) {
			return;
		}
		$this->UnInstallModule();
	}
}